/*
@Time : 2018/6/13 上午11:02
@Author : tengjufeng
@File : sortstudy
@Software: GoLand
*/

package main

import (
	"fmt"
	"sort"
)

func intSort() {
	a := []int{1, 2, 9, 8, 6, 7, 4, 5, 23, 87, 56, 34}
	fmt.Println("=======整形排序=======前")
	fmt.Println(a)
	fmt.Println("=======整形排序=======后")
	//升序
	//sort.Ints(a)
	//降序
	sort.Sort(sort.Reverse(sort.IntSlice(a)))

	fmt.Println(a)

}

func stringSort() {
	s := []string{"nihao", "中国", "renmen", "人们"}
	fmt.Println("=======字符串排序=======前")
	fmt.Println(s)
	fmt.Println("=======字符串排序=======后")
	sort.Strings(s)

	fmt.Println(s)

}

func main() {
	intSort()
	stringSort()

	users := Users{
		{openid: "abc", nickname: "张三", headimgurl: "def", score: 34},
		{openid: "abc", nickname: "李四", headimgurl: "def", score: 87},
		{openid: "abc", nickname: "王五", headimgurl: "def", score: 74},
		{openid: "abc", nickname: "陈六", headimgurl: "def", score: 94},
	}
	fmt.Println("原始数据:")
	for _, v := range users {
		fmt.Println(v.nickname, "-", v.score)
	}

	//排序
	sort.Sort(sort.Reverse(users))
	fmt.Println("排序后:")
	for _, v := range users {
		fmt.Println(v.nickname, "-", v.score)
	}
	//如果排序完成
	if sort.IsSorted(users) {
		fmt.Println("排序后:")
		for _, v := range users {
			fmt.Println(v.nickname, "-", v.score)
		}
	} else {
		fmt.Println("未完成排序")
	}
}

type User struct {
	openid     string
	nickname   string
	headimgurl string
	score      int
}

type Users []User

//实现sort 获取长度接口
func (this Users) Len() int {
	return len(this)
}

//按照nickname从小到大排序
func (this Users) Less(i, j int) bool {
	return this[i].score < this[j].score
}

//实现sort交换接口
func (this Users) Swap(i, j int) {
	this[i], this[j] = this[j], this[i]
}
